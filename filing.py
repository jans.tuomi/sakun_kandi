import os
uid = 1

# Placeholder Filing class


class Filing:
    def __init__(self, cik, filing_type, count):
        self.cik = cik
        self.filing_type = filing_type
        self.count = count

    def save(self, filings_dir):
        global uid
        out_dir = os.path.join(filings_dir, self.cik, "10-k")
        out_filename = os.path.join(out_dir, "file_{}.out".format(uid))
        uid += 1
        os.makedirs(out_dir, exist_ok=True)
        with open(out_filename, "w") as f:
            f.write("<html>ribul</html>")
